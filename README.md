## Synopsis
This is a guide to getting white-fang.py up and running.
Once installed, options are available via *python /path/to/white-fang.py -h.*

## Important
**You need to install and run this program on the University of Arizona HPC.**

## Installation of Dependencies

### Set Up Environment
**NOTE: Environments can be set up differently depending on preferences. These instructions are based on this environment. Some end-user troubleshooting is to be expected, as many variables can affect the dependency process. If you have any troubles, you can contact Asher (ahaug@email.arizona.edu) who will attempt to help you.**

$ ssh user@login.hpc.arizona.edu

[user@serviceX ~] pwd

**/home/uXX/user/ (FOR REMAINING STEPS, SUBSTITUTE THIS WITH SPECIFIC PATH)**

[user@serviceX ~] mkdir library

[user@serviceX ~] vim .bashrc

    # .bashrc
    export PATH=/home/uXX/user/library/bin:$PATH
    export LD_LIBRARY_PATH=/home/uXX/user/library/lib:$LD_LIBRARY_PATH
    export INCLUDE=/home/uXX/user/library/include:$INCLUDE

[user@serviceX ~] mkdir repos

### Install Makeflow & WorkQueue
[user@serviceX ~] cd repos

[user@serviceX repos]  wget http://ccl.cse.nd.edu/software/files/cctools-5.2.2-source.tar.gz

[user@serviceX repos]  tar xvzf cctools-5.2.2-source.tar.gz && cd cctools-5.2.2-source

[user@serviceX cctools-5.2.2-source] ./configure --prefix=/home/uXX/user/library

[user@serviceX cctools-5.2.2-source] make

[user@serviceX cctools-5.2.2-source] make install

[user@serviceX cctools-5.2.2-source] cd

### Install CFITSIO
[user@serviceX ~] cd repos

[user@serviceX repos] curl -LO ftp://heasarc.gsfc.nasa.gov/software/fitsio/c/cfitsio_latest.tar.gz

[user@serviceX repos] tar xvfz cfitsio_latest.tar.gz && cd cfitsio

[user@serviceX cfitsio] ./configure --prefix=/home/uXX/user/library

[user@serviceX cfitsio] make

[user@serviceX cfitsio] make install

[user@serviceX cfitsio] cd

### Install PyFits and Python-magic
[user@serviceX ~] module load python

[user@serviceX ~] pip install --user pyfits

[user@serviceX ~] pip install --user python-magic

### Install Astrometry.net
[user@serviceX ~] cd repos

[user@serviceX repos] curl -LO http://astrometry.net/downloads/astrometry.net-0.50.tar.bz2

[user@serviceX repos] tar xvfj astrometry.net-0.50.tar.bz2 && cd astrometry.net-0.50

[user@serviceX astrometry.net-0.50] make CFITS_INC="-I/home/uXX/user/library/include" CFITS_LIB="-L/home/uXX/user/library/lib -lcfitsio"

[user@serviceX astrometry.net-0.50] module load python

[user@serviceX astrometry.net-0.50] make py

[user@serviceX astrometry.net-0.50] make install INSTALL_DIR=/home/uXX/user/library

[user@serviceX astrometry.net-0.50] cd

[user@serviceX astrometry.net-0.50] cp /usr/bin/pnmfile /home/uXX/user/library/bin

### Get Astrometry.net Index Images
[user@serviceX ~] mkdir repos/astro_index && cd repos/astro_index

[user@serviceX ~] wget http://broiler.astrometry.net/~dstn/4200/wget.sh

[user@serviceX ~] sh wget.sh

[user@serviceX ~] vim ~/library/etc/astrometry.cfg
    (MODIFY add_path to the path of your index images)
    ~ add_path=/home/uXX/user/repos/astro_index

### Clone Project White-Fang
[user@serviceX ~] cd repos

[user@serviceX repos] git clone https://bitbucket.org/wolfsquadron/white-fang

## White-Fang Usage

### The Basics
python /path/to/white-fang.py [mode] [options]

#### Modes
--prepare

--makeflow

--makecfg

#### Options
**For the most part, if you follow the "simple usage" instructions, you can ignore most of the following options (except -t). Options are largely for "advanced" usage.**

-t </path/to/temporary/storage>
REQUIRED: ./ (works when executing from inside data folder)

-l <JSON of FITS to process>
DEFAULT: ./logs/final_fits_list.json (works when following simple usage)

### Simple Usage
From a folder that contains FITS files; files can be compressed and/or in subfolders.

#### 1. Prepare Directory
While in directory that contains your FITS files ("base directory"), call the following command:

$ python /path/to/whitefang-1 --prepare

#### 2. Make Makeflow file
After preparing the directory, call the following command:

$ python /path/to/whitefang-1 --makefile -t <path/to/temp/storage>

#### 3. Run Makeflow
The generated Makeflow file will be in a directory named "output". Change into this directory and then execute the Makeflow. There is some lenience here, the following directions should work in most circumstances.

$ cd output
$ makeflow whitefang.mf -T wq -N <project-name> --password <password-file>

You'll then need to start some workers, from a different shell. Modify and use the included PBS scripts (12astroworkers.pbs, 6astroworkersshared.pbs, 1astroworkershared.pbs). You can spin up as many as you want, but see my recommendations in the "Tips & Tricks" section.

$ qsub 12astroworkers.pbs

#### 4. Generate Astrometrica Config Files and Updated FITS Images
Change back to your "base directory"

$ cd ../

From the base directory, call the following command:

$ python /path/to/whitefang-1 --makecfg

This will generate an Astrometrica Config file and an updated FITS image for each solved image (these will be in the ./solved file).

To get a nice summary of the level of success, you can try out this command:

$ cat ./logs/makecfg_log.txt | grep "#"

(leave out the | grep "#" for more detailed info)

### Tips & Tricks
While this program is designed to work with massive sets of data, I suggest that users break their data into manageable chunks. This will make your life much easier in the end.

Some notes on workers: 
>I have provided three PBS scripts (for UA HPC, although may work on other PBS systems) that you can modify to get workers going. 12astroworkers.pbs will use a whole 12-core node, and start 12 workers. 6sharedastroworkers and 1sharedastroworker will share nodes with other users, and will start 6 or 1 worker respectively.

>I recommend 12astroworkers.pbs, as it will ensure others working on the system do not interfere with your analyses.

>When using the UA HPC system, starting many many workers (500+) seems to cause many jobs to timeout. I've had better luck using 50-100 at a time.

>Workers can also be started on other systems, but you need to ensure there is:

>>A functional Astrometry.net installation on that system, including index files.

>>A valid path EQUAL to what is specified in the --makeflow step (through the -t option)

If you already have a JSON object that lists paths to FITS files, you can skip the --prepare step, just use the -l (lower case L) option to specify this JSON file. If it was generated using the --prepare step, there is no need to specify (it knows where to look).

Edited online