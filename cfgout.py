__author__ = 'franka1, asherkhb'

def write_configs(filevalues):
    """
    Astrometrica Configuration File Writer
    - Given a dictionary of filename: {parameters} ['filevalues' - see below].
    - Write out an Astrometrica config file (filename.cfg) for each file in dictionary.
    - filevalues = { fname1: {"ra_dc": ra_dc,
                             "pixel_scale": pixel_scale,
                             "field_center": field_c,
                             "field_rotation": field_r,
                             "objctra": objctra,
                             "objctdec": objctdec
                            },
                     ...
                   }

    :param filevalues: dictionary of filename: {parameters} [see below], write out an Astrometrica config file.
    :return: True when complete
    """
    # TODO:May sometimes need to append YYYYMMDD to the name.
    for filename in filevalues:
        with open(filename + '.cfg', 'w') as f:
            focal_length = (206265*0.03)/filevalues[filename]["pixel_scale"]
            config_string = ('[Settings]\n'
                             'ApertureRadius=6\n'
                             'SigmaLimit=9.0\n'
                             'MinFWHM=2.00\n'
                             'FitRMS=0.50\n'
                             'SearchRadius=2.00\n'
                             'BkGround=1\n'
                             'Catalog=7\n'
                             'MinMag=14.0\n'
                             'MaxMag=10.0\n'
                             'PosResidual=2.00\n'
                             'MagResidual=0.10\n'
                             'FitOrder=1\n'
                             'NoMatch=500\n'
                             'MatchRadius=9.00\n'
                             'Distortion=0.00\n'
                             'NoAlign=50\n'
                             'SplitLine=1\n'
                             'IncludeMag=1\n'
                             'ExtraDigit=0\n'
                             'ExtraDigitMag=0\n'
                             'PixelWide=0.0300\n'
                             'PixelHigh=0.0300\n'
                             'Saturation=60000\n'
                             'SkipCheck=1\n'
                             'AutoRotate=0\n'
                             'SaveWCS=1\n'
                             'Color=V\n'
                             'Filter=V\n'
                             'FocalLength=' + str(focal_length) + '\n' +
                             'VarFocalLen=1.0\n'
                             'PA=' + str(filevalues[filename]["field_rotation"]) + '\n' +
                             'VarPA=2.0\n'
                             'Pointing=10.0\n'
                             'FlipHoriz=0\n'
                             'FlipVert=0\n'
                             'TimeMode=0\n'
                             'TimePrecision=1.00\n'
                             'TimeOffset=0.00\n'
                             'ExpTimeMode=1\n'
                             'CheckAtStart=1\n'
                             'AskOnClose=1\n'
                             'Color0=255\n'
                             'Color1=12632256\n'
                             'Color2=16711680\n'
                             'Color3=65280\n'
                             'Color4=65535\n'
                             'Color5=255\n'
                             'Color6=16711935\n'
                             '[ObservingSite]\n'
                             'MPCCode=290\n'
                             'Longitude=-110.7550\n'
                             'Latitude=32.4422\n'
                             'Height=2510.0\n'
                             'IncludeContact=1\n'
                             'Contact1=Carl W Hergenrother\n'
                             'Contact2=\n'
                             'eMail=chergen@lpl.arizona.edu\n'
                             'Observer=C. W. Hergenrother\n'
                             'Measurer=C. W. Hergenrother\n'
                             'Telescope=1.8-m reflector + CCD\n'
                             'TelescopeID=\n'
                             '[Ephemeris]\n'
                             'DeltaT=68.0\n['
                             'Paths]\n'
                             'CCDDir=Y:\ \n'
                             'MPCOrbDir=C:\Program Files\Astrometrica\Tutorials\n'
                             'OutDir=C:\Users\Carl\AppData\Local\Astrometrica\ \n'
                             '[Catalogs]USNOA2Dir=C:\Users\Carl\AppData\Local\Astrometrica\ \n'
                             'USNOB1Dir=C:\Users\Carl\AppData\Local\Astrometrica\ \n'
                             'UCAC3Dir=C:\Users\Carl\AppData\Local\Astrometrica\ \n'
                             'UCAC4Dir=C:\Program Files\Astrometrica\ \n'
                             'CMC14Dir=C:\Users\Carl\AppData\Local\Astrometrica\ \n'
                             'PPMXLDir=C:\Program Files\Astrometrica\ \n'
                             'USNOB1Mode=0\n'
                             'UCAC3Mode=0\n'
                             'UCAC4Mode=0\n'
                             'CMC14Mode=0\n'
                             'PPMXLMode=0\n'
                             '[Internet]\n'
                             'MailAdr=\n'
                             'MailServer=\n'
                             'MailPort=25\n'
                             'MailLogin=\n'
                             'MailPwd=\n'
                             'CCAdress1=\n'
                             'CCAdress2=\n'
                             'CCAdress3=\n'
                             'CCAdress4=\n'
                             'CCAdress5=\n'
                             'ProxyServer=\n'
                             'ProxyPort=80\n'
                             'MPCAdr=obs@cfa.harvard.edu\n'
                             'MPCServer=http://www.minorplanetcenter.org\n'
                             'MPCLogin=\n'
                             'MPCPassword=\n'
                             'MPCOrbPath=/iau/MPCORB\n'
                             'MPCOrbFile=MPCORB.DAT\n'
                             'CMTOrbFile=CometEls.txt\n'
                             'DLYOrbFile=DAILY.DAT\n'
                             'NEAOrbFile=NEAm00.txt\n'
                             'DSTOrbFile=Distant.txt\n'
                             'UNUOrbFile=Unusual.txt\n'
                             'VizierServer=vizier.cfa.harvard.edu\n'
                             'VizierPath=/viz-bin/VizieR\n'
                             'VizierMax=-9999\n'
                             '[Program]\n'
                             'Version=4.8.2.405\n'
            )
            f.write(config_string)
    return True