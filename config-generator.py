#!/usr/bin/env python

import argparse
from datetime import datetime
import magic
import os
import pyfits
from pprint import pprint
from decompress import *
from cfgout import *

"""
white-fang: A high-throughput approach for generating Astrometrica configuration files.
"""

__author__ = 'asherkhb, Alby Chaj, and Alex Frank'
__copyright__ = 'Copyright 2015, Wolf Squadron'
__credits__ = ['Asher Baltzell', 'Alby Chaj', 'Alex Frank', 'Daniel Chan', 'Kelly Novak']

__license__ = 'MIT'
__version__ = '0.1'
__maintainer__ = 'asherkhb'
__email__ = 'ahaug@email.arizona.edu'
__status__ = "Development"

# #-------------------# #
# Function Declarations #
# #-------------------# #


def determine_success(directory):
    success = []
    for root, dirs, files in os.walk(directory):
        for name in files:
            base = os.path.basename(name)
            fparts = os.path.splitext(base)
            if fparts[1] == '.solved':
                success.append(name)

    return success


def parse_astro_out(astropy_output_file):
    """
    This function takes in the astrometry.net output and returns the 
    filename as well as the parameters specified here:
        https://pods.iplantcollaborative.org/wiki/display/ACIC2015/How+to+run+astrometry.net+and+generate+Astrometrica+config+file

    Returns:
        2-tuple
    """
    if debug:
        print "Parsing %s" % astropy_output_file

    ra_dc = 'ERROR'
    pixel_scale = 0.1
    field_c = 'ERROR'
    field_r = 'ERROR'
    objctra = 'ERROR'
    objctdec = 'ERROR'

    outfile = open(astropy_output_file)
    for line in outfile: 
        line = line.strip()
        if line.startswith('RA,Dec'): # plate scale
            line_tup = line.rpartition(',') 
            ra_dc = line_tup[0].split('=')[-1].strip()
            pixel_scale = float(line_tup[-1].split()[-2].strip())
        if line.startswith('Field center: (RA H:M:S, Dec D:M:S)'): # field-center
            field_c = line.split('=')[-1].strip('.')
            objctra = field_c.strip('( )').split(',')[0].strip()
            objctdec = field_c.strip('( )').split(',')[1].strip()
        if line.startswith('Field rotation angle'): # field rotation
            field_r = line.split()[-5].strip()
    outfile.close()
    filename = os.path.splitext(astropy_output_file)[0]
    values = {"ra_dc": ra_dc,
              "pixel_scale": pixel_scale,  # For Config ("plate scale")
              "field_center": field_c,
              "field_rotation": field_r,  # For Config (PA)
              "objctra": objctra,
              "objctdec": objctdec}
    return filename, values


def get_astro_values(valid_files):
    vals = {}
    if debug:
        print "Getting astrometry.net values..."
    for f in valid_files:
        base = os.path.basename(f)
        fnew = os.path.splitext(base)[0] + ".stdout"
        stdout = args.i + fnew
        fname, fvals = parse_astro_out(stdout)
        vals[fname] = fvals
    return vals


def write_summary(successes, failures):
    summary_file = os.path.abspath(args.o) + '/_white-fang-summary.txt'
    with open(summary_file, 'w') as out:
        out.write("Successes:\n")
        for f in successes:
            out.write(">" + f + '\n')
        out.write("Failures:\n")
        for f in failures:
            out.write(">" + f + '\n')


# #----------------------------# #
# Process Command Line Arguments #
# #----------------------------# #

parser = argparse.ArgumentParser(description="White-Fang",
                                 epilog="For more help, visit TODO")
parser.add_argument('-i',
                    type=str,
                    default="./",
                    help="path to image folder")
parser.add_argument("--debug",
                    action="store_true",
                    help="enable debug mode")

args = parser.parse_args()
debug = args.debug

# #--------# #
# Run Script #
# #--------# #

start= datetime.now()
#if not os.path.exists(args.o):
#    os.makedirs(args.o)

#valid, invalid = process_file_list(args.i)
#if debug:
#    print "Valid Files: " + ", ".join(valid)
#    print "Invalid Files: " + ", ".join(invalid)
#generate_makeflow(valid)

#spawn_pbs_workers("genomeanalytics", 4)
#launch_makeflow("white-fang.mf", "wf_test", args.password)

success = determine_success(args.i)
critical_values = get_astro_values(success)
write_configs(critical_values)

#fail.extend(invalid)
#write_summary(success, fail)
end = datetime.now()
duration = end - start
print "Total Execution Time: %s" % str(duration)