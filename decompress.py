"""
Functions for decompression.
"""
import os
from zipfile import ZipFile
import bz2, tarfile

def decompress_zip(compressed_file):
    """
    Decompresses zip files uzing the zip module and extracts files to 
    current working directory.
    Valid extensions include: zip, zipx

    Documentation:
        https://docs.python.org/2/library/zipfile.html

    Returns:
        lst
    """
    file_lst = []
    with ZipFile(compressed_file) as zf:
        file_lst = zf.namelist()
        zf.extractall()
    os.system("rm '%s'" % (compressed_file))
    zip_name = os.path.splitext(compressed_file)[0].lstrip("./")
    for i in range(len(file_lst)):
        old_fname = os.path.splitext(file_lst[i])
        new_fname = old_fname[0] + zip_name + old_fname[1]
        if os.path.isfile(file_lst[i]):
            os.rename(file_lst[i], new_fname)
            file_lst[i] = new_fname
    return file_lst


def decompress_gzip(compressed_file):
    """
    Decompresses gzip files using the gzip module. File is added to 
    current working directory. 
    Valid extensions include: gz

    Documentation: 
        https://docs.python.org/2/library/tarfile.html

    Returns:
        lst
    """
    file_lst = []
    with tarfile.open(compressed_file, mode='r:gz') as tf:
        file_lst = tf.getnames()
        tf.extractall()
    os.system('rm "%s"' % (compressed_file))
    targz_name = os.path.splitext(compressed_file)[0].lstrip("./")
    for i in range(len(file_lst)):
        old_fname = os.path.splitext(file_lst[i])
        new_fname = old_fname[0] + targz_name + old_fname[1]
        if os.path.isfile(file_lst[i]):
            os.rename(file_lst[i], new_fname)
            file_lst[i] = new_fname
    return file_lst


def decompress_bzip2(compressed_file):
    """
    Decompresses bzip2 files.
    Valid extensions include: bz2

    Documentation: 
        https://docs.python.org/2/library/bz2.html

    Returns:
        lst
    """
    file_lst = []
    filename = os.path.splitext(compressed_file)[0]
    bf = bz2.BZ2File(compressed_file, 'rb')
    s = bf.read()
    output_file = file(filename, 'wb')
    output_file.write(s)
    output_file.close()
    file_lst.append(filename)
    os.system("rm '%s'" % (compressed_file))
    return file_lst


def decompress_tar(compressed_file):
    """
    Decompresses tar files using tarfile module.
    Valid extensions include: tar

    Documentation:
        https://docs.python.org/2/library/tarfile.html

    Returns:
        lst
    """
    file_lst = []
    with tarfile.open(compressed_file) as tf:
        file_lst = tf.getnames()
        tf.extractall()
    os.system("rm '%s'" % (compressed_file))
    tar_name = os.path.splitext(compressed_file)[0].lstrip("./")
    for i in range(len(file_lst)):
        old_fname = os.path.splitext(file_lst[i])
        new_fname = old_fname[0] + tar_name + old_fname[1]
        if os.path.isfile(file_lst[i]):
            os.rename(file_lst[i], new_fname)
            file_lst[i] = new_fname
    return file_lst


def decompress_gtar(compressed_file):
    """
    Decompresses gtar files.
    Valid extensions include: tgz, tar.Z, tar.bz2, tbz2, tar.lzma, tlz
    """
    file_lst = []
    with tarfile.open(compressed_file, mode='r:gz') as tf:
        file_lst = tf.getnames()
        tf.extractall()
    os.system("rm '%s'" % (compressed_file))
    return file_lst


def decompress_rar(compressed_file):
    """
    Decompresses rar files. 
    Valid extensions include: rar
    """
    return []


def decompress_7z(compressed_file):
    """
    Decompresses 7z files.
    Valid extensions include: 7z, s7z

    Might be possible solution: 
        https://github.com/fancycode/pylzma/blob/master/py7zlib.py
    """
    return []