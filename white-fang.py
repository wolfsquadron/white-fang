#!/usr/bin/env python

import argparse
from datetime import datetime
import magic
import os
import pyfits
from pprint import pprint
from decompress import *
from cfgout import *

"""
white-fang: A high-throughput approach for generating Astrometrica configuration files.
"""

__author__ = 'asherkhb, Alby Chaj, and Alex Frank'
__copyright__ = 'Copyright 2015, Wolf Squadron'
__credits__ = ['Asher Baltzell', 'Alby Chaj', 'Alex Frank', 'Daniel Chan', 'Kelly Novak']

__license__ = 'MIT'
__version__ = '0.1'
__maintainer__ = 'asherkhb'
__email__ = 'ahaug@email.arizona.edu'
__status__ = "Development"

# #-------------------# #
# Function Declarations #
# #-------------------# #


def process_file_list(img_directory):
    """
    This function should look through a directory and identify the valid/invalid FITS files.
    It must...
    - Identify compressed files, then decompress them. Remember, compression may hold more than one file.
    --> use unzip_file()
    - Validate every file.
    --> use validate_fits()
    - Generate lists of valid and invalid files, which will then be returned for downstream use.
    :param img_directory:
    :return: list of valid files, list of invalid files
    """
    # MIME-type constants
    compressed_mimes = ['x-gzip', 'x-bzip2', 'x-tar', 'x-gtar', 'zip', 'x-rar-compressed', 'x-7z-compressed']
    fits_mime = 'octet-stream'

    # Working arrays
    contents = {}  # currently unnecessary, but could be used if needed.
    compressed = []
    putative = []
    unknown = []

    # Return arrays
    valid_files = []
    invalid_files = []

    if debug:
        print "Walking the input directory..."

    for root, dirs, files in os.walk(img_directory):
        if debug:
            print "Analyzing files..."
        for name in files:
            f = os.path.join(root, name)
            mime = magic.from_file(f, mime=True)
            contents[f] = mime
            ftype = mime.split('/')[1]
            if ftype == fits_mime:
                if " " in list(os.path.basename(name)):
                    unknown.append(f)
                else:
                    putative.append(os.path.abspath(name))
            elif ftype in compressed_mimes:
                compressed.append(f)
            else:
                unknown.append(f)

    # Decompress compressed files.
    if debug:
        print "Decompressing compressed files..."
    decompress_log = []
    decompress_failures = []
    for c in compressed:
        if debug:
            print "Decompressing %s..." % c
        try:
            dc = unzip_file(c)
        except:
            decompress_log.append("Error - could not decompress %s.\n" % (c))
            decompress_failures.append(c)
        else:
            decompress_log.append("Successful decompression of %s.\n" % (c))
            if debug:
                print "Analyzing decompressed files..."
            for name in dc:
                f = os.path.abspath(name)
                mime = magic.from_file(f, mime=True)
                contents[f] = mime
                ftype = mime.split('/')[1]
                if ftype == fits_mime:
                    if " " in list(os.path.basename(name)):
                        unknown.append(f)
                    else:
                        putative.append(os.path.abspath(name))
                elif ftype in compressed_mimes:
                    compressed.append(f)
                else:
                    unknown.append(f)
    for name in decompress_failures:
        f = os.path.abspath(name)
        unknown.append(f)

    # Validate putative FITS files.
    for p in putative:
        if validate_fits(p):
            valid_files.append(p)
        else:
            invalid_files.append(p)

    # Add any remaining invalid files to invalid_files
    invalid_files.extend(unknown)

    print "Valid: " + ", ".join(valid_files)
    print "Invalid: " + ", ".join(invalid_files)
    return valid_files, invalid_files


def unzip_file(compressed_file):
    """
    This function unzips compressed files, handling most/all common 
    compressions. Also equipped to handle compressed directories 
    (i.e. tarballs)

    :param compressed_file: file to be decompressed
    :return: list of files that were decompressed
    """
    decompress = {
        'zip': decompress_zip,
        'x-gzip': decompress_gzip,
        'x-bzip2': decompress_bzip2,
        'x-tar': decompress_tar,        
        'x-gtar': decompress_gtar, 
        'x-rar-compressed': decompress_rar,
        'x-7z-compressed': decompress_7z,
    }
    mime = magic.from_file(compressed_file, mime=True)
    ftype = mime.split('/')[1]
    decompressed_files = decompress[ftype](compressed_file)

    return decompressed_files


def validate_fits(possible_fits):
    """
    Validate whether possible_fits file meets the FITS standard.
    Useful: http://stsdas.stsci.edu/download/wikidocs/The_PyFITS_Handbook.pdf
    :param possible_fits: FITS (or not) file.
    :return: Boolean
    """
    if debug:
        print "Validating %s" % possible_fits
    try:
        fit = pyfits.open(possible_fits)
        fit.verify()
        # TODO: This currently warns from verify, but still classifies them as "valid"
        if debug:
            print "Valid"
        return True
    except:
        if debug:
            print "Invalid"
        return False


def spawn_makeflow_entry(program, fits_file):
    """

    :param program:
    :param fits_file:
    :return:
    """
    if debug:
        print "Spawning Makeflow Entry for %s" % fits_file
    u = 'app'
    L = '0.3'
    H = '3.0'
    cfg = args.backendcfg
    #stop_otpts = '-R none -W none -M none -B none -p'
    stop_otpts = '-R none -W none -M none -B none'
    out_dir = os.path.abspath(args.o)

    base = os.path.basename(fits_file)
    otptbase = os.path.splitext(base)[0]
    stdout = out_dir + '/' + otptbase + '.stdout'
    #otpts = '{0}.axy {0}-indx.xyls {0}.new {0}.solved'.format(otptbase)
    #line_1 = '%s: %s %s\n' % (otpts, program, fits_file)  # Outputs Listed

    line_1 = ': %s %s\n' % (program, fits_file)
    line_2 = '\tmodule load python && %s -u %s -L %s -H %s %s --backend-config %s -D %s --cpulimit 600--skip-solved --overwrite %s > %s \n\n' % (program, u, L, H, stop_otpts, cfg, out_dir, fits_file, stdout)
    entry = line_1 + line_2
    return entry


def generate_makeflow(valid_files):
    """
    This function should take a list of valid FITS files and output a makeflow script to perform Astometry.net
    :param valid_files:
    :return:
    """
    if debug:
        print "Generating Makeflow..."
    astrometry = args.astrometry
    mf_head = "ASTROMETRY=%s\n\n" % astrometry
    with open('white-fang.mf', 'w') as mf:
        mf.write(mf_head)
        for fits_file in valid_files:
            mf.write(spawn_makeflow_entry("$ASTROMETRY", fits_file))
    return True


def launch_makeflow(makeflow_file, worker_name, worker_password_file):
    if debug:
        print "Launching makeflow..."
    command = 'makeflow -T wq %s -N %s --password %s' % (makeflow_file, worker_name, worker_password_file)
    os.system(command)


def spawn_pbs_workers(group, workernumber):
    if debug:
        print "Spawing WorkQueue Workers..."
    command = 'pbs_submit_workers -N wf_test -P %s -p "-N wf_test -W group_list=%s -q windfall -l jobtype=small_mpi -l select=1:ncpus=2:mem=3gb -l place=pack:shared -l walltime=01:00:00 -l cput=01:00:00" %s' % (args.password, group, str(workernumber))
    os.system(command)


def determine_success(valid_files, output_directory):
    solved = []
    success = []
    fail = []
    for root, dirs, files in os.walk(output_directory):
        for name in files:
            base = os.path.basename(name)
            fparts = os.path.splitext(base)
            if fparts[1] == '.solved':
                solved.append(fparts[0])
    for name in valid_files:
        base = os.path.basename(name)
        fparts = os.path.splitext(base)
        if fparts[0] in solved:
            success.append(name)
        else:
            fail.append(name)

    return success, fail


def parse_astro_out(astropy_output_file):
    """
    This function takes in the astrometry.net output and returns the 
    filename as well as the parameters specified here:
        https://pods.iplantcollaborative.org/wiki/display/ACIC2015/How+to+run+astrometry.net+and+generate+Astrometrica+config+file

    Returns:
        2-tuple
    """
    if debug:
        print "Parsing %s" % astropy_output_file

    ra_dc = 'ERROR'
    pixel_scale = 0.1
    field_c = 'ERROR'
    field_r = 'ERROR'
    objctra = 'ERROR'
    objctdec = 'ERROR'

    outfile = open(astropy_output_file)
    for line in outfile: 
        line = line.strip()
        if line.startswith('RA,Dec'): # plate scale
            line_tup = line.rpartition(',') 
            ra_dc = line_tup[0].split('=')[-1].strip()
            pixel_scale = float(line_tup[-1].split()[-2].strip())
        if line.startswith('Field center: (RA H:M:S, Dec D:M:S)'): # field-center
            field_c = line.split('=')[-1].strip('.')
            objctra = field_c.strip('( )').split(',')[0].strip()
            objctra = ' '.join(objctra.split(':'))
            objctdec = field_c.strip('( )').split(',')[1].strip()
            objctdec = ' '.join(objctdec.split(':'))
        if line.startswith('Field rotation angle'): # field rotation
            field_r = line.split()[-5].strip()
    outfile.close()
    filename = os.path.splitext(astropy_output_file)[0]
    values = {"ra_dc": ra_dc,
              "pixel_scale": pixel_scale,  # For Config ("plate scale")
              "field_center": field_c,
              "field_rotation": field_r,  # For Config (PA)
              "objctra": objctra,
              "objctdec": objctdec}
    return filename, values


def get_astro_values(valid_files):
    vals = {}
    if debug:
        print "Getting astrometry.net values..."
    for f in valid_files:
        base = os.path.basename(f)
        fnew = os.path.splitext(base)[0] + ".stdout"
        stdout = os.path.abspath(args.o) + "/" + fnew
        fname, fvals = parse_astro_out(stdout)
        vals[fname] = fvals
    return vals


def write_summary(successes, failures):
    summary_file = os.path.abspath(args.o) + '/_white-fang-summary.txt'
    with open(summary_file, 'w') as out:
        out.write("Successes:\n")
        for f in successes:
            out.write(">" + f + '\n')
        out.write("Failures:\n")
        for f in failures:
            out.write(">" + f + '\n')


# #----------------------------# #
# Process Command Line Arguments #
# #----------------------------# #

parser = argparse.ArgumentParser(description="White-Fang",
                                 epilog="For more help, visit TODO")
parser.add_argument('-i',
                    type=str,
                    default="./",
                    help="path to image folder")
parser.add_argument('-o',
                    type=str,
                    default="./white-fang-output/",
                    help="output directory")
parser.add_argument('--astrometry',
                    type=str,
                    required=True,
                    help="path to astrometry.net installation")
parser.add_argument('--backendcfg',
                    type=str,
                    required=True,
                    help="path to astrometry.net backend config file")
parser.add_argument('--password',
                    type=str,
                    required=True,
                    help="path to makeflow password file")
parser.add_argument("--debug",
                    action="store_true",
                    help="enable debug mode")

args = parser.parse_args()
debug = args.debug

# #--------# #
# Run Script #
# #--------# #

start= datetime.now()
if not os.path.exists(args.o):
    os.makedirs(args.o)

valid, invalid = process_file_list(args.i)
if debug:
    print "Valid Files: " + ", ".join(valid)
    print "Invalid Files: " + ", ".join(invalid)
generate_makeflow(valid)

#spawn_pbs_workers("genomeanalytics", 4)
launch_makeflow("white-fang.mf", "wf_test", args.password)

success, fail = determine_success(valid, args.o)
#if debug:
    #print "Success: " + ", ".join(success)
    #print "Fail: " + ", ".join(fail)

critical_values = get_astro_values(success)
write_configs(critical_values)

fail.extend(invalid)
write_summary(success, fail)
end = datetime.now()
duration = end - start
print "Total Execution Time: %s" % str(duration)