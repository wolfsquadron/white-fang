__author__ = 'asherkhb'

import argparse
from datetime import datetime
from decompress import *
import json
import magic
import pyfits
import shutil


def build_file_structure(list_of_files):
    for f in list_of_files:
        if not os.path.exists(f):
            os.makedirs(f)


def identify_compressed(input_directory):
    global compressed_files
    compressed_mimes = ['application/x-gzip',
                        'application/x-bzip2',
                        'application/x-tar',
                        'application/x-gtar',
                        'application/zip',
                        'application/x-rar-compressed',
                        'application/x-7z-compressed']

    for root, dirs, files in os.walk(input_directory):
        files = [f for f in files if not f[0] == '.']  # Skip hidden files
        dirs[:] = [d for d in dirs if not d[0] == '.']  # Skip hidden dirs
        dirs[:] = [d for d in dirs if not d == 'errors']  # Skip contents of "errors" folder
        # for dir in dirs:
        #     print os.path.join(root, dir)
        for f in files:
            fn = os.path.join(root, f)
            mime = magic.from_file(fn, mime=True)
            if mime in compressed_mimes:
                compressed_files.append(fn)


def unzip_file(compressed_file):
    """
    This function unzips compressed files, handling most/all common
    compressions. Also equipped to handle compressed directories
    (i.e. tarballs)

    :param compressed_file: file to be decompressed
    :return: list of files that were decompressed
    """
    decompress = {
        'zip': decompress_zip,
        'x-gzip': decompress_gzip,
        'x-bzip2': decompress_bzip2,
        'x-tar': decompress_tar,
        'x-gtar': decompress_gtar,
        'x-rar-compressed': decompress_rar,
        'x-7z-compressed': decompress_7z,
    }
    mime = magic.from_file(compressed_file, mime=True)
    ftype = mime.split('/')[1]
    decompressed_files = decompress[ftype](compressed_file)

    return decompressed_files


def strip_spaces(dir):
    # Rename all subforders/files of dir, excluding dir itself
    def rename_all(root, items):
        for name in items:
            try:
                os.rename( os.path.join(root, name), os.path.join(root, name.replace(' ', '_').replace('(', '').replace(')', '').replace('"', '').replace("'", "")))
            except OSError:
                pass  # For those that cannot be renamed

    # starts from the bottom so paths further up remain valid after renaming
    for root, dirs, files in os.walk(dir, topdown=False):
        files = [f for f in files if not f[0] == '.']  # Skip hidden files
        dirs[:] = [d for d in dirs if not d[0] == '.']  # Skip hidden dirs
        rename_all(root, dirs)
        rename_all(root, files)


def analyze_files(input_directory):
    analysis_log = {'fits': [], 'dark': [], 'nofits': [], 'skipped': []}
    fits_mime = 'application/octet-stream'
    for root, dirs, files in os.walk(input_directory):
        # Skip hidden files & directories
        files = [f for f in files if not f[0] == '.']  # Skip hidden files
        dirs[:] = [d for d in dirs if not d[0] == '.']  # Skip hidden dirs
        # Skip "bins" (prevents redundant processing)
        dirs[:] = [d for d in dirs if not d == 'fits']
        dirs[:] = [d for d in dirs if not d == 'darks']
        dirs[:] = [d for d in dirs if not d == 'nofits']
        dirs[:] = [d for d in dirs if not d == 'logs']
        dirs[:] = [d for d in dirs if not d == 'errors']
        dirs[:] = [d for d in dirs if not d == 'output']
        dirs[:] = [d for d in dirs if not d == 'solutions']
        for name in files:
            f = os.path.join(root, name)
            # print "...analyzing %s" % f
            mime = magic.from_file(f, mime=True)
            if mime == fits_mime:
                try:
                    fit = pyfits.open(f)
                    head = fit[0].header
                    try:
                        obsdate = head['DATE-OBS'].split('T')[0].replace('-', '').replace("'", "").replace(' ', '')
                    except:
                        obsdate = "NODATE"
                    try:
                        vimtype = head['VIMTYPE']
                    except:
                        vimtype = 'NA'
                    fit.close()

                    fbase = os.path.basename(f)
                    fbasename = os.path.splitext(fbase)[0]
                    new_base = fbasename + '_' + str(obsdate) + '.FITS'
                    new_f = os.path.join(root, new_base)
                    os.rename(f, new_f)

                    if vimtype == 'DARK':
                        try:
                            analysis_log['dark'].append(new_f)
                            shutil.move(new_f, dark_dir)
                        except:
                            print "Error with %s...skipping" % new_f
                            analysis_log['skipped'].append(new_f)
                    else:
                        try:
                            analysis_log['fits'].append(new_f)
                            shutil.move(new_f, fits_dir)
                        except:
                            print "Error with %s...skipping" % new_f
                            analysis_log['skipped'].append(new_f)
                except:
                    try:
                        analysis_log['nofits'].append(f)
                        shutil.move(f, nofits_dir)
                    except:
                        print "Error with %s...skipping" % f
                        analysis_log['skipped'].append(f)
            else:
                try:
                    analysis_log['nofits'].append(f)
                    shutil.move(f, nofits_dir)
                except:
                    print "Error with %s...skipping" % f
                    analysis_log['skipped'].append(f)

    return analysis_log


def build_fits_list(fits_dir, output_file):
    fits_list = []
    # List all FITS files in fits_dir, add to fits_list
    filelst = os.listdir(fits_dir)
    for f in filelst:
        fitsf = os.path.join(fits_dir, f)
        fits_list.append(fitsf)
    # Dump fits_list to a .json object
    with open(output_file, 'w') as o:
        json.dump(fits_list, o)

    return fits_list


def spawn_makeflow_entry(fits_file):
    fbase = os.path.basename(fits_file)
    ftitle = os.path.splitext(fbase)[0]
    line1 = './%s/: .%s\n' % (ftitle, fits_file)
    line2 = '@FITSF="%s"\n' % ftitle
    line3 = '\tmkdir -p $FITSF && timeout 10m solve-field -u app -L 0.3 -H 3.0 --dir ./$FITSF --temp-dir $TEMP --skip-solved .%s >> $FITSF/$FITSF.stdout\n\n' % fits_file
    entry = line1 + line2 + line3
    return entry


def extract_cfg_vals(filename):
    """
    This function takes in the astrometry.net standard output and returns the parameters specified here:
        https://pods.iplantcollaborative.org/wiki/display/ACIC2015/How+to+run+astrometry.net+and+generate+Astrometrica+config+file

    Returns:
        dictionary of values
    """
    astropy_output_file = './output/%s/%s.stdout' % (filename, filename)
    outfile = open(astropy_output_file)
    for line in outfile:
        line = line.strip()
        if line.startswith('RA,Dec'): # plate scale
            line_tup = line.rpartition(',')
            ra_dc = line_tup[0].split('=')[-1].strip()
            pixel_scale = float(line_tup[-1].split()[-2].strip())
        if line.startswith('Field center: (RA H:M:S, Dec D:M:S)'): # field-center
            field_c = line.split('=')[-1].strip('.')
            objctra = field_c.strip('( )').split(',')[0].strip()
            objctra = ' '.join(objctra.split(':'))
            objctdec = field_c.strip('( )').split(',')[1].strip()
            objctdec = ' '.join(objctdec.split(':'))
        if line.startswith('Field rotation angle'): # field rotation
            field_r = line.split()[-5].strip()
    outfile.close()
    values = {"ra_dc": ra_dc,
              "pixel_scale": pixel_scale,  # For Config ("plate scale")
              "field_center": field_c,
              "field_rotation": field_r,  # For Config (PA)
              "objctra": objctra,
              "objctdec": objctdec}
    return values

def write_config(filename, filevalues):
    """
    Astrometrica Configuration File Writer
    - Given a filename and dictionary of parameters
    - Write out an Astrometrica config file (./solutions/filename.cfg).
    - filevalues = {"ra_dc": ra_dc,
                    "pixel_scale": pixel_scale,
                    "field_center": field_c,
                    "field_rotation": field_r,
                    "objctra": objctra,
                    "objctdec": objctdec
                    },

    :param filename: name of file.
    :param filevalues: dictionary of file values.
    :return: True when complete
    """
    # TODO:May sometimes need to append YYYYMMDD to the name.
    cfg_name = './solutions/%s.cfg' % filename
    with open(cfg_name, 'w') as f:
        focal_length = (206265*0.03)/filevalues["pixel_scale"]
        config_string = ('[Settings]\n'
                         'ApertureRadius=6\n'
                         'SigmaLimit=9.0\n'
                         'MinFWHM=2.00\n'
                         'FitRMS=0.50\n'
                         'SearchRadius=2.00\n'
                         'BkGround=1\n'
                         'Catalog=7\n'
                         'MinMag=14.0\n'
                         'MaxMag=10.0\n'
                         'PosResidual=2.00\n'
                         'MagResidual=0.10\n'
                         'FitOrder=1\n'
                         'NoMatch=500\n'
                         'MatchRadius=9.00\n'
                         'Distortion=0.00\n'
                         'NoAlign=50\n'
                         'SplitLine=1\n'
                         'IncludeMag=1\n'
                         'ExtraDigit=0\n'
                         'ExtraDigitMag=0\n'
                         'PixelWide=0.0300\n'
                         'PixelHigh=0.0300\n'
                         'Saturation=60000\n'
                         'SkipCheck=1\n'
                         'AutoRotate=0\n'
                         'SaveWCS=1\n'
                         'Color=V\n'
                         'Filter=V\n'
                         'FocalLength=' + str(focal_length) + '\n' +
                         'VarFocalLen=1.0\n'
                         'PA=' + str(filevalues["field_rotation"]) + '\n' +
                         'VarPA=2.0\n'
                         'Pointing=10.0\n'
                         'FlipHoriz=0\n'
                         'FlipVert=0\n'
                         'TimeMode=0\n'
                         'TimePrecision=1.00\n'
                         'TimeOffset=0.00\n'
                         'ExpTimeMode=1\n'
                         'CheckAtStart=1\n'
                         'AskOnClose=1\n'
                         'Color0=255\n'
                         'Color1=12632256\n'
                         'Color2=16711680\n'
                         'Color3=65280\n'
                         'Color4=65535\n'
                         'Color5=255\n'
                         'Color6=16711935\n'
                         '[ObservingSite]\n'
                         'MPCCode=290\n'
                         'Longitude=-110.7550\n'
                         'Latitude=32.4422\n'
                         'Height=2510.0\n'
                         'IncludeContact=1\n'
                         'Contact1=Carl W Hergenrother\n'
                         'Contact2=\n'
                         'eMail=chergen@lpl.arizona.edu\n'
                         'Observer=C. W. Hergenrother\n'
                         'Measurer=C. W. Hergenrother\n'
                         'Telescope=1.8-m reflector + CCD\n'
                         'TelescopeID=\n'
                         '[Ephemeris]\n'
                         'DeltaT=68.0\n['
                         'Paths]\n'
                         'CCDDir=Y:\ \n'
                         'MPCOrbDir=C:\Program Files\Astrometrica\Tutorials\n'
                         'OutDir=C:\Users\Carl\AppData\Local\Astrometrica\ \n'
                         '[Catalogs]USNOA2Dir=C:\Users\Carl\AppData\Local\Astrometrica\ \n'
                         'USNOB1Dir=C:\Users\Carl\AppData\Local\Astrometrica\ \n'
                         'UCAC3Dir=C:\Users\Carl\AppData\Local\Astrometrica\ \n'
                         'UCAC4Dir=C:\Program Files\Astrometrica\ \n'
                         'CMC14Dir=C:\Users\Carl\AppData\Local\Astrometrica\ \n'
                         'PPMXLDir=C:\Program Files\Astrometrica\ \n'
                         'USNOB1Mode=0\n'
                         'UCAC3Mode=0\n'
                         'UCAC4Mode=0\n'
                         'CMC14Mode=0\n'
                         'PPMXLMode=0\n'
                         '[Internet]\n'
                         'MailAdr=\n'
                         'MailServer=\n'
                         'MailPort=25\n'
                         'MailLogin=\n'
                         'MailPwd=\n'
                         'CCAdress1=\n'
                         'CCAdress2=\n'
                         'CCAdress3=\n'
                         'CCAdress4=\n'
                         'CCAdress5=\n'
                         'ProxyServer=\n'
                         'ProxyPort=80\n'
                         'MPCAdr=obs@cfa.harvard.edu\n'
                         'MPCServer=http://www.minorplanetcenter.org\n'
                         'MPCLogin=\n'
                         'MPCPassword=\n'
                         'MPCOrbPath=/iau/MPCORB\n'
                         'MPCOrbFile=MPCORB.DAT\n'
                         'CMTOrbFile=CometEls.txt\n'
                         'DLYOrbFile=DAILY.DAT\n'
                         'NEAOrbFile=NEAm00.txt\n'
                         'DSTOrbFile=Distant.txt\n'
                         'UNUOrbFile=Unusual.txt\n'
                         'VizierServer=vizier.cfa.harvard.edu\n'
                         'VizierPath=/viz-bin/VizieR\n'
                         'VizierMax=-9999\n'
                         '[Program]\n'
                         'Version=4.8.2.405\n'
        )
        f.write(config_string)
    return True


def make_final_fits(filename, filevalues):
    new_fit = "./output/%s/%s.new" % (filename, filename)
    final_fit = "./solutions/%s.FITS" % filename
    shutil.copy2(new_fit, final_fit)
    try:
        fit = pyfits.open(final_fit, mode='update')
        prihdr = fit[0].header
        prihdr.set('objctra', filevalues['objctra']) # should we add comment associated with keyword?
        prihdr.set('objctdec', filevalues['objctdec'])
        # ra, dec? not the ones specified by field center but the other (i.e. fname['ra_dc'])
        # pixel scale? only used for calculating focal length? if we use, remember to cast
        # field rotation?
        # above keywords can also be added to header - need to ask
        prihdr['history'] = 'Lovingly updated by Wolf Squadron' # comment vs. history - probably history
        fit.flush()
        fit.close()
        return True
    except:
        return False


# def make_final_fits(filename, filevalues):
#     new_fit = "./output/%s/%s.new" % (filename, filename)
#     final_fit = "./solutions/%s.FITS" % filename
#     shutil.copy2(new_fit, final_fit)
#     fit = pyfits.open(final_fit, mode='update')
#     prihdr = fit[0].header
#     prihdr.set('objctra', filevalues['objctra']) # should we add comment associated with keyword?
#     prihdr.set('objctdec', filevalues['objctdec'])
#     # ra, dec? not the ones specified by field center but the other (i.e. fname['ra_dc'])
#     # pixel scale? only used for calculating focal length? if we use, remember to cast
#     # field rotation?
#     # above keywords can also be added to header - need to ask
#     prihdr['history'] = 'Lovingly updated by Wolf Squadron' # comment vs. history - probably history
#     fit.flush()
#     fit.close()
#     return True


# #----------------------------# #
# Process Command Line Arguments #
# #----------------------------# #

parser = argparse.ArgumentParser(description="White-Fang",
                                 epilog="For more help, see readme.md")
parser.add_argument("--prepare",
                    action="store_true",
                    help="MODE: prepare working directory for analysis")
parser.add_argument("--makeflow",
                    action="store_true",
                    help="MODE: generate a makeflow file from list of FITS")
parser.add_argument("--makecfg",
                    action="store_true",
                    help="MODE: write astrometrica config files from a directory of solved FITS")
parser.add_argument('-l',
                    type=str,
                    default="./logs/final_fits_list.json",
                    help="path to image folder")
parser.add_argument('-t',
                    type=str,
                    required=True,
                    help="path to temp storage")

args = parser.parse_args()

# #--------------------# #
# Execute Script Actions #
# #--------------------# #

start = datetime.now()

input_dir = './'
fits_dir = './fits'
dark_dir = './darks'
nofits_dir = './nofits'
log_dir = './logs'
error_dir = './errors'
output_dir = './output'
solution_dir = './solutions'


if args.prepare:
    error_log = {}

    # Build necessary file structure
    print "Preparing white-fang environment"
    build_file_structure([fits_dir, dark_dir, nofits_dir, log_dir, error_dir, output_dir, solution_dir])

    # Find & extract any compressed files, recursively.
    print "Extracting compressed files"
    decompress_log = {"success": [], "fail": []}
    compressed_files = []
    identify_compressed(input_dir)
    for i in range(len(compressed_files)):
        cf = compressed_files.pop()
        print "...extracting %s" % cf
        try:
            unzip_file(cf)
            decompress_log["success"].append(cf)  # Adds successfully decompressed file to log
        except:
            error_log[cf] = "Unable to decompress"  # Adds unzippable file to error log
            decompress_log["fail"].append(cf)  # Adds unzippable file to decompress log
            shutil.move(cf, error_dir)  # Move unzippable file to 'errors' bin

    # Write out decompression log.
    print "Writing extraction log"
    complogf = log_dir + '/decompression_log.txt'
    with open(complogf, 'w') as log_out:
        log_out.write("# FAILED TO EXTRACT (moved to " + error_dir + ")\n")
        for f in decompress_log['fail']:
            log_out.write(f + '\n')
        log_out.write("# SUCCESSFULLY EXTRACTED\n")
        for f in decompress_log['success']:
            log_out.write(f + '\n')

    # Remove all spaces from file/folders
    print "Stripping spaces from any file/directory names"
    strip_spaces(input_dir)

    # Identify, validate, and consolidate fits files.
    print "Analyzing files"
    analysis_log = analyze_files(input_dir)

    # Write out analysis log.
    print "Writing analysis log"
    analysislogf = log_dir + '/analysis_log.txt'
    with open(analysislogf, 'w') as log_out:
        log_out.write("# IDENTIFIED " + str(len(analysis_log['fits'])) + " FITS (moved to " + fits_dir + ")\n")
        for f in analysis_log['fits']:
            log_out.write(f + '\n')

        log_out.write("# IDENTIFIED " + str(len(analysis_log['dark'])) + " DARKS (moved to " + dark_dir + ")\n")
        for f in analysis_log['dark']:
            log_out.write(f + '\n')

        log_out.write("# IDENTIFIED " + str(len(analysis_log['nofits'])) + " NON-FITS (moved to " + nofits_dir + ")\n")
        for f in analysis_log['nofits']:
            log_out.write(f + '\n')

        log_out.write("# SKIPPED " + str(len(analysis_log['skipped'])) + " FILES\n")
        for f in analysis_log['skipped']:
            log_out.write(f + '\n')

    # Build & export list (JSON) of FITS files to extract
    fits_json = log_dir + '/final_fits_list.json'
    print "Building FITS list, saving as %s" % fits_json
    final_fits_list = build_fits_list(fits_dir, fits_json)


if args.makeflow:
    build_file_structure([fits_dir, dark_dir, nofits_dir, log_dir, error_dir, output_dir, solution_dir])
    print "Generating a makeflow from %s" % str(args.l)
    print "Loading FITS file list..."
    with open(args.l, 'r') as lin:
        flist = json.load(lin)
    with open('./output/whitefang.mf', 'w') as mf:
        mf.write('TEMP=' + args.t + '\n\n')
        for fits in flist:
            mf.write(spawn_makeflow_entry(fits))

    print "Makeflow Generated (./output/whitefang.mf)"


if args.makecfg:
    cfglog = {"solved": [], "unsolved": [], "success": [], "fitsupdate": [], "noupdate": [], "errors": {}}
    entry_list = []
    print "Loading FITS list (%s)..." % args.l
    with open(args.l, 'r') as lin:
        flist = json.load(lin)
    for f in flist:
        fbase = os.path.basename(f)
        fn = os.path.splitext(fbase)[0]
        entry_list.append(fn)
    print "Writing configuration files to solved directory..."
    for f in entry_list:
        try:
            fdir = './output/%s' % f
            cnts = os.listdir(fdir)
            if f + '.solved' in cnts:
                cfglog["solved"].append(f)
                try:
                    cfgvals = extract_cfg_vals(f)
                    write_config(f, cfgvals)
                    cfglog["success"].append(f)
                    try:
                        stat = make_final_fits(f, cfgvals)
                        if stat:
                            cfglog["fitsupdate"].append(f)
                        else:
                            cfglog["errors"][f] = "Error updating new fits. RA/Dec may be incorrect."
                            cfglog["noupdate"].append(f)
                    except Exception as e:
                        cfglog["errors"][f] = "Error creating/updating final FITS - %s" % e
                except:
                    cfglog["errors"][f] = "Unable to find configuration values in stdout"
            else:
                cfglog["unsolved"].append(f)
        except:
            cfglog["errors"][f] = "No output directory identified"
            print "Error: Couldn't find an output directory for %s" % f

    # Write out configuration log.
    print "Writing analysis log"
    cfglogf = log_dir + '/makecfg_log.txt'
    with open(cfglogf, 'w') as log_out:
        log_out.write("# IDENTIFIED " + str(len(cfglog['solved'])) + " SOLVED\n")
        for f in cfglog['solved']:
            log_out.write(f + '\n')

        log_out.write("# IDENTIFIED " + str(len(cfglog['unsolved'])) + " UNSOLVED\n")
        for f in cfglog['unsolved']:
            log_out.write(f + '\n')

        log_out.write("# WROTE " + str(len(cfglog['success'])) + " CONFIGS (see " + solution_dir + ")\n")
        for f in cfglog['success']:
            log_out.write(f + '\n')

        log_out.write("# UPDATED " + str(len(cfglog['fitsupdate'])) + " FINAL FITS (see " + solution_dir + ")\n")
        for f in cfglog['fitsupdate']:
            log_out.write(f + '\n')

        log_out.write("# FAILED TO UPDATE " + str(len(cfglog['noupdate'])) + " FINAL FITS (see " + solution_dir + ")\n")
        for f in cfglog['noupdate']:
            log_out.write(f + '\n')

        log_out.write("# " + str(len(cfglog['errors'])) + " ERRORS\n")
        for f in cfglog['errors']:
            log_out.write(f + ": " + cfglog['errors'][f] + '\n')


end = datetime.now()
duration = end - start
print "Total Execution Time: %s" % str(duration)
